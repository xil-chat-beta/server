const tokenSender = reqlib("/listenners/tokenSender.js");

module.exports = (io) => {
	// Token sender
	tokenSender.bind(io);

	// Connection handler
	io.on("connection", (socket) => {
		// TODO: Handle connection
		console.log("New socket connection at " + socket.id);

		socket.on("disconnect", (socket) => {
			console.log(`A client being disconnected`);
		});
	});
};
