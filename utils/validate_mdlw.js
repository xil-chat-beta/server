const { validationResult } = require("express-validator");
const errors_conf = reqlib("/config/errors.json");

module.exports.validate = (validations) => {
	return async (req, res, next) => {
		// Checks in routes handler, using validation_schemas
		await Promise.all(validations.map((validation) => validation.run(req)));

		// Get validations results and continue if no error found
		const errors = validationResult(req);
		if (errors.isEmpty()) {
			return next();
		}

		// Return all errors
		// const parsed_errors = errors.array().map((err) => {
		// 	return {
		// 		code: err.msg,
		// 		msg: errors_conf[err.msg.toString()],
		// 	};
		// });

		// const reduced_errors = parsed_errors.reduce(
		// 	(acc, cur) => (acc.find((itm) => itm.code == cur.code) ? acc : acc.concat(cur)),
		// 	[]
		// );

		// Return only one error
		const parsed_errors = {
			code: errors.array()[0].msg,
			msg: errors_conf[errors.array()[0].msg],
		};

		// Block request if data is invalid
		res.status(400).json({
			error: parsed_errors,
		});
	};
};
