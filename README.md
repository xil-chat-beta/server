# xil-chat-API

The API of the xil-chat project

## Installation

1. Clone this repository

```bash
git clone https://gitlab.com/xil-chat-beta/server.git
```

2. Install dependencies

```bash
yarn
```

3. Update preferencies
   
   **IMPORTANT: You have to create a secret string to sign tokens**

   Edit `config/config.json`

   Add your mysql connection details and setup your server port.

   Note: In later releases, I will make a script to make initialization faster.
   TODO: Add `.sql` initialization script.

4. Start the server

```bash
yarn start
```

## Report issue

Please help me to make this project better through issues in order for me to solve them.

## Thanks

Thanks [@Mistera91](https://github.com/Mistera91) for creating the Android mobile application.

<!-- This script will ask you mysql informations (if not in the `config/config.json` file, create the database and create a configuration file if required, by following the `config/config.example.json` template. -->
