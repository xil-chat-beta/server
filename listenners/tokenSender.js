const notifier = reqlib("/utils/Notifier.js");
const jwt = require("jsonwebtoken");
const { jsonwebtoken } = reqlib("/config/config.json");

// Listen to notifier events, called in ioserver.js
module.exports.bind = (io) => {
	notifier.addListener("socket_send_verification_token", (socketId) => {
		this.send_token(socketId, io);
	});
};

// Validate provided socket ID, Generate and send token
module.exports.send_token = async (socketId, io) => {
	try {
		if (!io.sockets.sockets.has(socketId))
			// Users can change their socket.id in login request
			return console.warn("SOCKET LISTENNER WARNING: Someone sent a bad socket id");

		const token = jwt.sign(
			{
				socket_id: socketId,
			},
			jsonwebtoken.secret,
			{ expiresIn: "1h" }
		);

		io.to(socketId).emit("tokenGenerated", { token: token });
		console.log(token);
	} catch (error) {
		console.error("SOCKET LISTENNER ERROR: Authentification token generation error\n\n", error);
	}
};
