// Init router
const express = require("express");
const router = express.Router();
const { validate } = reqlib("/utils/validate_mdlw");
const { checkSchema } = require("express-validator");

// Load controllers
const authCtrl = reqlib("/controllers/auth");

// Validator schema (All data verification config in config/settings.json)
const { registerSchema, loginSchema } = reqlib("/config/validation_schema");

// Routes
router.post("/register", validate([checkSchema(registerSchema)]), authCtrl.register);
router.post("/login", validate([checkSchema(loginSchema)]), authCtrl.login);

// Export router
module.exports = router;
