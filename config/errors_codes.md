# Error codes explaination

An error code is composed by 3 digits
The first one indicate where the problem come from, 0 is client and 1 is server.
The second digit is the part of the API which throwed an error, for example, the 0 refers to the authentication part.
The third digit indicate the error in his category, the list of errors can be found below.

---

## Table of content

-   `1x*` [Server errors](#server-errors-1xx)
    -   `0` [Database](#database-1)
-   `0x*` [Client errors](#client-errors-0xx)
    -   `0` [Authentication (TOKEN, session expire)](#authentication-00x)
    -   `1` [Login](#login-01x)
    -   `2` [Register](#register-02x)
    -   `3` [DMR](#dmr-03x)
    -   `4` [Message](#dmr-03x)

## List of errors

### Server errors (`1xx`)

#### Database (`0`)

-   `0` Database connexion error
-   `1` Database insert error

### Client errors (`0xx`)

#### Authentication (`00x`)

-   `0` Invalid token

#### Login (`01x`)

-   `0` Bad request
-   `1` Empty username
-   `2` Invalid username or password
-   `3` Invalid socketID
-   `4` Connection already used -- IF USED

#### Register (`02x`)

-   `1` Invalid username (type or length)
-   `2` Invalid password (strenght...)
-   `3` Username already exists
-   `4` Invalid public key

#### DMR (`03x`)
#### Messages (`04x`)
