module.exports.registerSchema = {
	username: {
		errorMessage: "021",
		isAscii: true,
		isLength: { options: global.settings.register.username_len },
		custom: {
			// If the username already exists
			options: async (value) => {
				if (value == undefined) return Promise.reject("021");

				const [rows] = await global.db.execute(
					"SELECT COUNT(*) AS user_exists FROM Users users WHERE username = :user",
					{ user: value.trim() }
				);
				if (rows[0].user_exists > 0) return Promise.reject("023");
				return Promise.resolve();
			},
		},
		trim: true,
	},
	password: {
		isStrongPassword: {
			// To configure the password policy, see config/settings.json
			options: global.settings.register.password_policy,
		},
		errorMessage: "022",
		trim: true,
	},
	pub_key: {
		// TODO: Configure others validators
		isHexadecimal: true,
		errorMessage: "024",
		trim: true,
	},
};

module.exports.loginSchema = {
	username: {
		notEmpty: { options: { ignore_whitespace: false }, errorMessage: "011" },
		errorMessage: "010",
		isAscii: true,
		trim: true,
	},
	password: {
		notEmpty: { options: { ignore_whitespace: false } },
		errorMessage: "010",
		trim: true,
	},
	socket_id: {
		errorMessage: "013",
		notEmpty: { options: { ignore_whitespace: false } },
	},
};
