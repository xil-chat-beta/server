const bcrypt = require("bcrypt");
const errors_conf = reqlib("/config/errors.json");
const success_msg = reqlib("/config/success_msg.json");
const notifier = reqlib("/utils/Notifier.js");

// Controllers
exports.register = async (req, res, next) => {
	// Data already validated and safe

	const pass_hash = await bcrypt.hash(
		req.body.password,
		global.settings.register.hash.salt_rounds
	);

	const user_data = {
		username: req.body.username,
		password_hash: pass_hash,
		pub_key: req.body.pub_key,
	};

	// Save user in DB and handle potential error
	try {
		await global.db.execute(
			"INSERT INTO Users(username, password_hash, pub_key) VALUES(:username, :password_hash, :pub_key)",
			user_data
		);
		res.status(201).json({ message: success_msg.auth.register_success });
	} catch (error) {
		res.status(500).json({
			error: {
				code: "001",
				msg: errors_conf["001"],
			},
		});
	}
};

exports.login = async (req, res, next) => {
	// Data already validated and safe

	try {
		const [user] = await global.db.execute("SELECT * FROM Users WHERE username = :username", {
			username: req.body.username,
		});

		// The user doesn't exists
		if (user.length <= 0) {
			return res.status(200).json({
				error: {
					code: "012",
					msg: errors_conf["012"],
				},
			});
		}

		// Password validity check
		try {
			const isPasswordValid = await bcrypt.compare(req.body.password, user[0].password_hash);
			if (!isPasswordValid) {
				return res.status(200).json({
					error: {
						code: "012",
						msg: errors_conf["012"],
					},
				});
			}

			// Send token via socket
			notifier.emit("socket_send_verification_token", req.body.socket_id);
			res.status(201).json({ message: success_msg.auth.login_success });
		} catch (err) {
			// Password validation error, send internal server error
			console.error(err);
			res.status(500).json({
				error: {
					code: "002",
					msg: errors_conf["002"],
				},
			});
		}
		// Wrong credentials
	} catch (error) {
		console.error(error);
		res.status(200).json({
			error: {
				code: "012",
				msg: errors_conf["012"],
			},
		});
	}
};
